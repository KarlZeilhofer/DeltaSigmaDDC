#include <iostream>
#include "deltasigmaddc.h"
#include <string.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char *argv[])
{
	int64_t min=0,max=UINT16_MAX;
	DeltaSigmaDDC ddc(min, max);

	int64_t input = 64000;
	int N = 10000;

	for(int i=1; i<argc; i++){
		if(strncmp(argv[i], "-N=", 3)==0){
			N = atoi(argv[i]+3);
		}else if(strncmp(argv[i], "-in=", 4)==0){
			input = atoi(argv[i]+4);
		}else if(strncmp(argv[i], "-min=", 5)==0){
			min = atoi(argv[i]+5);
		}else if(strncmp(argv[i], "-max=", 5)==0){
			max = atoi(argv[i]+5);
		}else if(strncmp(argv[i], "--help", 6)==0){
			cout << "example: delta-sigma-ddc -N=10000 -in=1234 -min=0 -max=10000" << endl;
		}
	}

	ddc.setLimits(min,max);

	for(int i=0; i<N; i++){
		cout << (int)ddc.step(input) << endl;
	}
	return 0;
}
