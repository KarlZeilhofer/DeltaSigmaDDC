#include "deltasigmaddc.h"

DeltaSigmaDDC::DeltaSigmaDDC(int64_t inputMin, int64_t inputMax)
{
	setLimits(inputMin, inputMax);
}

uint8_t DeltaSigmaDDC::step(int64_t input)
{
	int64_t diff;

	if(output){
		diff = input-DRef_plus;
	}else{
		diff = input-DRef_minus;
	}

	sum = sum + diff; // here is the time step

	output = (sum>compare)?1:0;

	return output;
}

uint8_t DeltaSigmaDDC::step(int64_t input, double dt)
{
	int64_t diff;

	if(output){
		diff = input-DRef_plus;
	}else{
		diff = input-DRef_minus;
	}

	sum = sum + diff*dt; // here is the time step

	output = (sum>compare)?1:0;

	return output;
}

void DeltaSigmaDDC::setLimits(int64_t min, int64_t max)
{
	DRef_minus = min;
	DRef_plus = max;
	compare = (min + max)/2;
}
