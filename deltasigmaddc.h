#ifndef DELTASIGMADAC_H
#define DELTASIGMADAC_H


#include <stdint.h>
#include <limits.h>

/**
 * @brief The DeltaSigmaDCC class implements a
 * digital to digital converter.
 *
 * In this case an uint16 to a 1-Bit signal
 *
 * Implements Figure 3 of this page:
 * http://www.beis.de/Elektronik/DeltaSigma/DeltaSigma_D.html
 */


class DeltaSigmaDDC
{
public:
	DeltaSigmaDDC(int64_t inputMin, int64_t inputMax);

	/**
	 * @brief step performs one time step
	 * @param input, 16-bit input value
	 * @return 1-bit output value
	 */
	uint8_t step(int64_t input);


	/**
	 * @brief step performs one time step
	 * @param input, 16-bit input value
	 * @param dt, variable timestep.
	 * this simulates an analog integrator
	 *
	 * @return 1-bit output value
	 */
	uint8_t step(int64_t input, double dt);

	void setLimits(int64_t min, int64_t max);

private:
	/// this equals the minimum input value:
	int64_t DRef_minus;

	/// this equals the maximum input value:
	int64_t DRef_plus;
	int64_t compare;

	int64_t sum=0;

public:
	uint8_t output=0; // 1-Bit output
};

#endif // DELTASIGMADAC_H
